# Software Studio 2019 Spring Assignment 2

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 
    算是分成三個階段，後面的敵人血會越來越厚，子彈數也會變多一次，而當地圖捲動到最後的時候，也會出現Boss。
    而玩家也可以在上方中間看到大招的集氣值，當到100的時候就可以按R來開大招。開大招可以看到攻擊速度明顯加快，上方中間也會變成剩餘時間。
2. Animations : 
    就簡單的左移跟右移時玩家會有不同動畫。
3. Particle Systems : 
    加入兩種不同的pixel，會在敵人被打中時噴發粒子效果。
    另外敵人打到玩家的爆炸則是選擇用動畫的方式呈現。算是上面的Animation的一種。
4. Sound effects : 
    有分成三種音樂，一開始的背景音樂，玩家被打中時的爆炸聲以及Boss出場時的音樂。
5. Leaderboard : 
    會在最下面的地方可以輸入名字，而名字下方就可以看到目前記錄。要注意的是當第一名被取代，原本的第一名第二名應該要取代下去。
6. UI : 
    這邊大概介紹一下如何遊戲，首先會看到分面，也就是Menu State，可以看到兩行字，按下任意方向鍵進入遊戲，或者按空白鍵看遊戲說明。
    雖然遊戲說明大致說完功能了，不過在這邊在講一次:
    進入遊戲後可以在上方分別看到Score、Ultimate能量、Lives。
    利用Q & W 控制音量，ESC 進行暫停。控制音量自動顯示目前音量大小，一段時間後消失，並利用toPrecision(2)進行近似值。

# Bonus Functions Description : 
1. Boss: 
    會自動跟著玩家，並噴一堆子彈。有夠猛。
2. Enhanced items:
    隨著遊戲進行，會自動跳出一些功能可以吃，共有三種寫在下面
3. Bullet automatic aiming:
    'A'box-> 產生兩個紫色子彈自動瞄準第一個敵人。
4. Unique bullet:
    'M'box-> 產生多兩排子彈
5. Little helper:
    'H'box-> 多兩個小幫手。

#註
如果撞到敵機會直接死亡。
其實只要靠在最旁邊就可以避免被子彈打中。方便我測試XD
遊戲打完Boss並不會結束，可以繼續刷分到你爽XD
最下面有render function可以用來debug很好用




