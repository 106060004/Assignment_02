var LeaderBoardF;
var menuState = {
    preload: function(){

    },
    create: function(){
        this.bg = game.add.image(0, 0, 'bg_menu');
        this.bg.alpha = 0.9;
        //Check after player die the bound will still correct
        game.world.setBounds(0, 0, 640, 600);
        //Display the name of this game
        /*var nameLabel = game.add.text(game.width/2, 200, 'Raiden Game', {font: '50px Arial', fill: '#000000'});
        nameLabel.anchor.setTo(0.5, 0.5);*/
        //Explain how to play
        this.ctlLabel = game.add.text(game.width/2, 150, 'Press any arrow key to start', {font: '40px Arial', fill: '#ffffff'});
        this.ctlLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(this.ctlLabel.scale).to({x: 1.1, y: 1.1}, 1000).yoyo(true).loop().start();
        //show the leaderboard
        this.docLabel = game.add.text(game.width/2, 550   , 'Press space key for description', {font: '32px Dosis', fill: '#ffffff'});
        this.docLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(this.docLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
        //description
        this.description = game.add.text(game.width/2, 250, 'Use arrow key to move, space key to shoot.\n Use Q and W key to control the volume.\n Use R key to use Ulimate and ESC key to pause.', {font: '24px Dosis', fill: '#ffffff'})
        this.description.anchor.setTo(.5, .5);
        this.description.alpha = 0;
        this.description2 = game.add.text(game.width/2, 350, 'Please check the following which is about your name and leaderboard.', {font: '24px Dosis', fill: '#ffffff'})
        this.description2.anchor.setTo(.5, .5);
        this.description2.alpha = 0;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        firebase.database().ref('LeaderBoard/First').once('value').then(function(snapshot){
            document.getElementById('firstName').innerText = snapshot.val().name;
            document.getElementById('firstScore').innerText = snapshot.val().score;
        });
        firebase.database().ref('LeaderBoard/Second').once('value').then(function(snapshot){
            document.getElementById('secondName').innerText = snapshot.val().name;
            document.getElementById('secondScore').innerText = snapshot.val().score;
        });
        firebase.database().ref('LeaderBoard/Third').once('value').then(function(snapshot){
            document.getElementById('thirdName').innerText = snapshot.val().name;
            document.getElementById('thirdScore').innerText = snapshot.val().score;
        });

    },
    update: function(){
        
        if(this.cursor.up.isDown || this.cursor.down.isDown || this.cursor.left.isDown || this.cursor.right.isDown)
            game.state.start('play', false, true);
        
        if(this.spaceKey.isDown){
            this.bg.alpha = 0.2;
            this.ctlLabel.alpha = 0.2;
            this.docLabel.alpha = 0.2;
            this.description.alpha = 1;
            this.description2.alpha = 1;
        }else{
            this.bg.alpha = 0.9;
            this.ctlLabel.alpha = 1;
            this.docLabel.alpha = 1;
            this.description.alpha = 0;
            this.description2.alpha = 0;
        }
    },
};