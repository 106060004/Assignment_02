//for animation
var testanimation_r = 0;
var testanimation_l = 0;
//bulletTime
var bulletTime = 0;
var bossBulletTime = 0;
//worldBound
var worldBound = {
    x: 0,
    y: 536//remember to adjust
}
//first time boss
var first = 0;
//vol conttrol Time
var volTime = 0;
//score
var score = 0;
//ultimate
var ultimate = 0;
var ultimateTime = false;
var ultduration = 0;
//aiming bullet time
var aimingBulletTime = 0

var first_500 = true;
var first_400 = true;
var first_300 = true;
var playState = {
    preload: function(){

    },
    create: function(){
        score = 0;
        //add background
        this.background = game.add.tileSprite(0, 0, 640, 1136, 'background');
        game.world.setBounds(0, 536, 640, 1136);//the y bounds height need to set 1136, which means greater than 1000, 
                                                //but I don't know why.
                                                //Because of the collideWorldBounds!!!

        //add player
        this.player = game.add.sprite(game.width/2, 1000, 'player');
        this.player.anchor.setTo(0.5, 0.5);
        this.player.scale.setTo(0.5, 0.5);
        this.player.lives = 10;
        //animation
        this.player.animations.add('rightfly', [2, 3, 4], 8, false);
        this.player.animations.add('leftfly', [2, 1, 0], 8, false);
        this.player.frame = 2;
        //physics
        game.physics.arcade.enable(this.player);
        this.player.body.setSize(40, 60, 80, 60);
        this.player.body.collideWorldBounds = true;//Let the player be always in world
        //bouns
        this.player.bounsM = false;
        this.player.bounsA = false;
        this.player.bounsH = false;
        
        //add lives
        this.livesLabel = game.add.text(640, worldBound.y, 'Lives: '+this.player.lives , {font: '20px Arial', fill: '#ffffff'});
        this.livesLabel.anchor.setTo(1, 0); 
        //add score
        this.scoreLabel = game.add.text(0, worldBound.y, 'Score: '+score , {font: '20px Arial', fill: '#ffffff'});


        //add camera
        /*game.camera.follow(this.player);*/
        //game.camera.y = 0;
        //use camera to fade or shake!!!

        //control the worldbound to go forward
        game.time.events.loop(80, this.forward, this);

        //add arrowkeys
        this.cursor = game.input.keyboard.createCursorKeys();

        //add spacekey
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.spaceKey.onDown.add(this.firebullet, this); 

        //add esc key
        this.escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        this.escKey.onDown.add(this.pause, this);
        this.pauseLabel = game.add.text(game.width/2, worldBound.y+330, 'PAUSE~', {font: '32px Dosis', fill: '#ffffff'})
        this.pauseLabel.anchor.setTo(.5, .5);
        this.pauseLabel.alpha = 0;

        //add control vol key;
        this.volup = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        this.voldown = game.input.keyboard.addKey(Phaser.Keyboard.W);
        this.volup.onDown.add(this.VolUP, this);
        this.voldown.onDown.add(this.VolDOWN, this);

        //add bullet group
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(100, 'bullet');

        //add enemy group
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10, 'enemy');
        
        //add enemies per loop
        if(this.player.alive)
            game.time.events.loop(1500, this.addEnemy, this);

        //add enemy bullets group
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.createMultiple(100, 'enemyBullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 0.5);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);
        
        //add enemy bullets per loop
        game.time.events.loop(500, function(){
            this.enemies.forEach(function(enemy){
                if(enemy.alive){
                    if(worldBound.y < 500 && worldBound.y >= 300){
                        var enemyBullet = this.enemyBullets.getFirstDead();
                        if(!enemyBullet)
                            return;
                        enemyBullet.reset(enemy.x, enemy.y);
                        enemyBullet.body.velocity.y = 200;
                    }else if(worldBound.y < 300 && worldBound.y >0){
                        var enemyBullet = this.enemyBullets.getFirstDead();
                        if(!enemyBullet)
                            return;
                        enemyBullet.reset(enemy.x+10, enemy.y);
                        enemyBullet.body.velocity.y = 300;
                        //the second bullet
                        enemyBullet = this.enemyBullets.getFirstDead();
                        if(!enemyBullet)
                            return;
                        enemyBullet.reset(enemy.x-10, enemy.y);
                        enemyBullet.body.velocity.y = 300;
                    }else if(worldBound.y == 0){
                        //still fire enemy bullets
                        var enemyBullet = this.enemyBullets.getFirstDead();
                        if(!enemyBullet)
                            return;
                        enemyBullet.reset(enemy.x+10, enemy.y);
                        enemyBullet.body.velocity.y = 300;
                        //the second bullet
                        enemyBullet = this.enemyBullets.getFirstDead();
                        if(!enemyBullet)
                            return;
                        enemyBullet.reset(enemy.x-10, enemy.y);
                        enemyBullet.body.velocity.y = 300;
                        //add boss
                        if(first == 0){
                            first++;
                            this.boss.reset(game.width/2, 100);
                            this.bgm.stop();
                            this.bossBgm.play();
                            this.bossBgm.loop = true;
                        }
                    }
                }
            }, this);
        }, this);

        //add boss
        this.boss = game.add.sprite(game.width/2, 100, 'boss');
        this.boss.kill();
        this.boss.anchor.setTo(.5, .5);
        this.boss.lives = 50;
        game.physics.arcade.enable(this.boss);
        this.boss.body.collideWorldBounds = true;
        this.boss.angle = 180;
        this.boss.scale.setTo(1.5, 1.5);

        //add boss bullets group
        this.bossBullets = game.add.group();
        this.bossBullets.enableBody = true;
        this.bossBullets.createMultiple(100, 'bossBullet');
        //add boss bullets2 group
        this.bossBullets2 = game.add.group();
        this.bossBullets2.enableBody = true;
        this.bossBullets2.createMultiple(100, 'bossBullet2');

        //add pixel emitter
        this.emitter = game.add.emitter(0, 0, 500);
        this.emitter.makeParticles(['pixel_red','pixel_orange']);
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 0;
        //add explosion group
        this.explosions = game.add.group();
        this.explosions.createMultiple(30, 'explosion');
        this.explosions.forEach(this.setupExp, this);

        //add audio
        this.bgm = game.add.audio('bgm');
        this.bgm.play();
        this.bgm.loop = true;   
        this.explosion_player = game.add.audio('explosion_player');
        this.bossBgm = game.add.audio('boss_bgm');
        this.bgm.volume = 0.5;
        this.bossBgm.volume = 0.5;
        this.explosion_player.volume = 0.5;

        //add volumeLabel
        this.volLabel = game.add.text(game.width/2, worldBound.y+200, 'Volume: '+this.bgm.volume, {font: '32px Dosis', fill: '#ffffff'});
        this.volLabel.anchor.setTo(.5, .5);
        this.volLabel.alpha = 0;

        //add ultLabel
        this.ultLabel = game.add.text(game.width/2, 0, 'Ultimate: '+ultimate, {font: '24px Arial', fill: '#ffffff'});
        this.ultLabel.anchor.setTo(.5, 0);
        //add ultimate loop
        game.time.events.loop(100, function(){
            if(!ultimateTime)
                if(ultimate < 100)
                    ultimate++;
            this.ultLabel.text = "Ultimate: "+ultimate;
        }, this);
        //add ultimate key
        this.ultKey = game.input.keyboard.addKey(Phaser.Keyboard.R);
        this.ultKey.onDown.add(this.Ult, this);
        


        //add bouns function
        this.m_box = game.add.sprite(game.width/2, 600, 'm_box');
        this.m_box.anchor.setTo(.5, 0);
        this.m_box.kill();
        game.physics.arcade.enable(this.m_box);
        //this.m_box.body.collideWorldBounds = true;
        this.m_box.body.bounce.x = 1;
        this.m_box.body.bounce.y = 1;
        this.m_box.scale.setTo(0.1, 0.1);

        this.a_box = game.add.sprite(game.width/2, 500, 'a_box');
        this.a_box.anchor.setTo(.5, 0);
        this.a_box.kill();    
        game.physics.arcade.enable(this.a_box);
        //this.a_box.body.collideWorldBounds = true;
        this.a_box.body.bounce.x = 1;
        this.a_box.body.bounce.y = 1;
        this.a_box.scale.setTo(0.1, 0.1);
        
        this.h_box = game.add.sprite(game.width/2, 400, 'h_box');
        this.h_box.anchor.setTo(.5, 0);
        this.h_box.kill();    
        game.physics.arcade.enable(this.h_box);
        //this.h_box.body.collideWorldBounds = true;
        this.h_box.body.bounce.x = 1;
        this.h_box.body.bounce.y = 1;
        this.h_box.scale.setTo(0.1, 0.1);
        

        //check time to release the bonus
        game.time.events.loop(100, function(){
            if(worldBound.y < 500){
                if(first_500){
                    first_500 = false;
                    this.m_box.reset(game.width/2, 600);
                    this.m_box.body.velocity.x = game.rnd.pick([-1, 1])*100;
                    this.m_box.body.velocity.y = 100;
                }
            }if(worldBound.y < 480){
                this.m_box.body.collideWorldBounds = true;
            }
            if(worldBound.y < 400){
                if(first_400){
                    first_400 = false;
                    this.a_box.reset(game.width/2, 500);
                    this.a_box.body.velocity.x = game.rnd.pick([-1, 1])*100;
                    this.a_box.body.velocity.y = 100;
                }
            }if(worldBound.y < 380){
                this.a_box.body.collideWorldBounds = true;
            }
            if(worldBound.y < 300){
                if(first_300){
                    first_300 = false;
                    this.h_box.reset(game.width/2, 400);
                    this.h_box.body.velocity.x = game.rnd.pick([-1, 1])*100;
                    this.h_box.body.velocity.y = 100;
                }
            }if(worldBound.y < 280){
                this.h_box.body.collideWorldBounds = true;
            }
        }, this);

        //bounsM is in the firebullets function
        //bounsA
        this.aimingBullets = game.add.group();
        this.aimingBullets.enableBody = true;
        this.aimingBullets.createMultiple(100, 'aimingBullet');
        //bounsH
        this.helper_l = game.add.sprite(0, 0, 'helper');
        this.helper_l.kill();
        game.physics.arcade.enable(this.helper_l);
        this.helper_l.anchor.setTo(.5, .5);
        this.helper_l.scale.setTo(.5, .5);
        this.helper_l.lives = 5;
        game.time.events.loop(500, function(){
            if(this.helper_l.alive){
                var bullet = this.bullets.getFirstDead();
                if(!bullet)
                    return;
                bullet.anchor.setTo(0.5, 0.5);
                bullet.scale.setTo(.7, .7);
                bullet.reset(this.helper_l.x, this.helper_l.y-28);
                bullet.body.velocity.y = -200;
                bullet.checkWorldBounds = true;
                bullet.outOfBoundsKill = true;
            }
        }, this);
        this.helper_r = game.add.sprite(0, 0, 'helper');
        this.helper_r.kill();
        game.physics.arcade.enable(this.helper_r);
        this.helper_r.anchor.setTo(.5, .5);
        this.helper_r.scale.setTo(.5, .5);
        this.helper_r.lives = 5;
        game.time.events.loop(500, function(){
            if(this.helper_r.alive){
                var bullet = this.bullets.getFirstDead();
                if(!bullet)
                    return;
                bullet.anchor.setTo(0.5, 0.5);
                bullet.scale.setTo(.7, .7);
                bullet.reset(this.helper_r.x, this.helper_r.y-28);
                bullet.body.velocity.y = -200;
                bullet.checkWorldBounds = true;
                bullet.outOfBoundsKill = true;
            }
        }, this);

    },
    update: function(){
        if(this.player.alive){
            this.movePlayer();
            game.physics.arcade.overlap(this.player, this.enemies, this.playerDie, null, this);
            game.physics.arcade.overlap(this.enemies, this.bullets, this.enemyShot, null, this);
            game.physics.arcade.overlap(this.player, this.enemyBullets, this.playerShot, null, this);

            game.physics.arcade.overlap(this.player, this.boss, this.playerDie, null, this);
            game.physics.arcade.overlap(this.boss, this.bullets, this.bossShot, null, this);
            game.physics.arcade.overlap(this.player, this.bossBullets, this.playerShot, null, this);
            game.physics.arcade.overlap(this.player, this.bossBullets2, this.playerShot, null, this);

            //bouns
            game.physics.arcade.overlap(this.player, this.m_box, this.bouns, null, this);
            game.physics.arcade.overlap(this.player, this.a_box, this.bouns, null, this);
            game.physics.arcade.overlap(this.player, this.h_box, this.bouns, null, this);
            game.physics.arcade.overlap(this.helper_l, this.enemyBullets, this.helperShot, null, this);
            game.physics.arcade.overlap(this.helper_r, this.enemyBullets, this.helperShot, null, this);

            if(this.player.bounsA){
                this.fireaimingBullet();
            }
        }
        this.moveBoss();
        if(volTime > game.time.now){
            this.volLabel.alpha = 1;
        }else{
            this.volLabel.alpha = 0;
        }
        if(ultimateTime){
            ultduration++;
            this.ultLabel.text = "Last Time: "+((600-ultduration)/60).toPrecision(2);
            if(ultduration == 600){
                ultduration = 0;
                ultimateTime = false;
            }
        }
        this.checkAiming();
        game.physics.arcade.overlap(this.enemies, this.aimingBullets, this.enemyShot, null, this);
        this.moveHelper();
    },
    movePlayer: function(){
        //y direction
        if(this.cursor.up.isDown){
            this.player.body.velocity.y = -300;
        }else if(this.cursor.down.isDown){
            this.player.body.velocity.y = +300;
        }else{
            this.player.body.velocity.y = 0;
        }
        //x direction
        if(this.cursor.left.isDown){
            testanimation_r = 0;
            this.player.body.velocity.x = -300;
            if(testanimation_l == 0){
                this.player.animations.play('leftfly');
                testanimation_l = 1;
            }
        }else if(this.cursor.right.isDown){
            testanimation_l = 0;
            this.player.body.velocity.x = +300;
            if(testanimation_r == 0){
                this.player.animations.play('rightfly');
                testanimation_r = 1;
            }
        }else{
            testanimation_l = 0;
            testanimation_r = 0;
            this.player.body.velocity.x = 0;
            this.player.animations.stop();
            this.player.frame = 2;
        }
        //addtack
        if(this.spaceKey.isDown){
            this.firebullet();
        }
    },
    forward: function(){
        if(worldBound.y > 0)
            worldBound.y -= 1;
        game.world.setBounds(worldBound.x, worldBound.y, 640, 600);
        this.livesLabel.y = worldBound.y;
        this.scoreLabel.y = worldBound.y;
        this.volLabel.y = worldBound.y+200;
        this.pauseLabel.y = worldBound.y+250;
        this.ultLabel.y = worldBound.y;
    },
    firebullet: function(){
        if(bulletTime < game.time.now){
            if(this.player.bounsM){
                var bullet = this.bullets.getFirstDead();
                if(!bullet)
                    return ;
                bullet.anchor.setTo(0.5, 0.5);
                bullet.reset(this.player.x-20, this.player.y-28);
                bullet.body.velocity.y = -300;
                bullet.checkWorldBounds = true;
                bullet.outOfBoundsKill = true;
                bullet = this.bullets.getFirstDead();
                if(!bullet)
                    return ;
                bullet.anchor.setTo(0.5, 0.5);
                bullet.reset(this.player.x, this.player.y-28);
                bullet.body.velocity.y = -300;
                bullet.checkWorldBounds = true;
                bullet.outOfBoundsKill = true;
                bullet = this.bullets.getFirstDead();
                if(!bullet)
                    return ;
                bullet.anchor.setTo(0.5, 0.5);
                bullet.reset(this.player.x+20, this.player.y-28);
                bullet.body.velocity.y = -300;
                bullet.checkWorldBounds = true;
                bullet.outOfBoundsKill = true;
                if(ultimateTime)
                    bulletTime = game.time.now + 100;
                else
                    bulletTime = game.time.now + 200;
            }else{
                var bullet = this.bullets.getFirstDead();
                if(!bullet)
                    return ;
                bullet.anchor.setTo(0.5, 0.5);
                bullet.reset(this.player.x, this.player.y-28);
                bullet.body.velocity.y = -300;
                bullet.checkWorldBounds = true;
                bullet.outOfBoundsKill = true;
                if(ultimateTime)
                    bulletTime = game.time.now + 100;
                else
                    bulletTime = game.time.now + 200;
            }
        }
    },
    addEnemy: function(){
        var enemy = this.enemies.getFirstDead();
        if(!enemy)
            return;
        enemy.anchor.setTo(0.5, 0.5);
        let x = game.rnd.pick([150, 250, 350, 450]);
        enemy.reset(x, worldBound.y+50);
        enemy.body.velocity.x = 100*game.rnd.pick([-1, 1]);
        enemy.body.collideWorldBounds = true;
        enemy.body.bounce.x = 1
        enemy.angle = 180;
        if(worldBound.y == 0)
            enemy.lives = 30;
        else if(worldBound.y < 300)
            enemy.lives = 20;
        else if(worldBound.y < 536)
            enemy.lives = 10;
        enemy.body.setSize(100, 80, 5, 6);//adjust the collision box
        //fire enemyBullet
        /*game.time.events.loop(500, function(){
            if(enemy.alive){
                var enemyBullet = this.enemyBullets.getFirstDead();
                if(!enemyBullet)
                    return;
                enemyBullet.reset(enemy.x, enemy.y);
                enemyBullet.body.velocity.y = 200;
            }
            //do it again
        }, this);*/
    },
    playerDie: function(){
        game.camera.shake();
        //firebase
        if(document.getElementById('PlayerName').value != ""){
        var FisrtScore, SecondScore, ThirdScore;
        var FisrtName, SecondName, ThirdName;
        firebase.database().ref('LeaderBoard/First').once('value').then(function(snapshot1){
            FisrtScore = snapshot1.val().score;
            FisrtName = snapshot1.val().name;
            firebase.database().ref('LeaderBoard/Second').once('value').then(function(snapshot2){
                SecondScore = snapshot2.val().score;
                SecondName = snapshot2.val().name;
                firebase.database().ref('LeaderBoard/Third').once('value').then(function(snapshot3){
                    ThirdScore = snapshot3.val().score;
                    ThirdName = snapshot3.val().name;
                    if(score > FisrtScore){
                        firebase.database().ref('LeaderBoard/First').set({
                            name: document.getElementById("PlayerName").value,
                            score: score
                        });
                        firebase.database().ref('LeaderBoard/Second').set({
                            name: FisrtName,
                            score: FisrtScore
                        });
                        firebase.database().ref('LeaderBoard/Third').set({
                            name: SecondName,
                            score: SecondScore
                        });
                    }else if(score > SecondScore){
                        firebase.database().ref('LeaderBoard/Second').set({
                            name: document.getElementById("PlayerName").value,
                            score: score
                        });
                        firebase.database().ref('LeaderBoard/Third').set({
                            name: SecondName,
                            score: SecondScore
                        });
                    }else if(score > ThirdScore){
                        firebase.database().ref('LeaderBoard/Third').set({
                            name: document.getElementById("PlayerName").value,
                            score: score
                        });
                    }
                });
            });
        });
        }
        //intital var
        this.bullets.callAll('kill', this);
        this.enemyBullets.callAll('kill', this);
        this.enemies.callAll('kill', this);
        this.boss.kill();
        this.player.kill();
        this.bgm.stop();
        this.bossBgm.stop();
        this.explosion_player.stop();
        //for animation
        testanimation_r = 0;
        testanimation_l = 0;
        //bulletTime
        bulletTime = 0;
        bossBulletTime = 0;
        //worldBound
        worldBound = {
            x: 0,
            y: 536
        }
        first = 0;
        volTime = 0;
        //  score = 0;
        ultimate = 0;
        ultimateTime = false;
        ultduration = 0;
        //aiming bullet time
        aimingBulletTime = 0

        first_500 = true;
        first_400 = true;
        first_300 = true;
        //wait and go back to menu
        game.time.events.add(1000, function(){
            game.state.start('menu');
        }, this);
    },
    enemyShot: function(enemy, bullet){
        this.emitter.x = bullet.x;
        this.emitter.y = bullet.y;
        this.emitter.start(true, 800, null, 10);
        
        bullet.kill();
        enemy.lives--;
        if(enemy.lives <= 0){
            enemy.kill();
            score += 200;
            this.scoreLabel.text = "Score: "+score;
        }   
    },
    playerShot: function(player, enemyBullet){
        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(enemyBullet.x, enemyBullet.y);
        explosion.play('explosion', 24, false, true);

        this.explosion_player.play();

        enemyBullet.kill();
        player.lives--;
        this.livesLabel.text = "Lives: "+this.player.lives;
        if(player.lives <= 0){
            this.playerDie();
        }
    },
    helperShot: function(helper, enemyBullet){
        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(enemyBullet.x, enemyBullet.y);
        explosion.play('explosion', 24, false, true);

        this.explosion_player.play();

        enemyBullet.kill();
        helper.lives--;
        if(helper.lives <= 0){
            helper.kill()
        }
    },
    moveBoss: function(){
        //special move
        if(this.player.x > this.boss.x+10){
            this.boss.body.velocity.x = 150;
        }else if(this.player.x < this.boss.x-10){
            this.boss.body.velocity.x = -150;
        }else{
            this.boss.body.velocity.x = 0;
        }
        //fire bossBullets
        if(this.boss.alive){
            if(bossBulletTime < game.time.now){
                var bullet = this.bossBullets.getFirstDead();
                if(!bullet)
                    return ;
                bullet.anchor.setTo(0.5, 0.5);
                bullet.angle = 180;
                bullet.scale.setTo(2, 2);
                bullet.reset(this.boss.x, this.boss.y+30);
                bullet.body.velocity.y = 200;
                bullet.checkWorldBounds = true;
                bullet.outOfBoundsKill = true;
                bossBulletTime = game.time.now + 300;
                //bullets2
                for(var i=0 ; i<10 ; i++){
                    var bullet = this.bossBullets2.getFirstDead();
                    if(!bullet)
                        return ;
                    bullet.anchor.setTo(0.5, 0.5);
                    bullet.scale.setTo(1.5, 1.5);
                    bullet.reset(this.boss.x, this.boss.y);
                    bullet.body.velocity.x = (i-4.5)*60;
                    bullet.body.velocity.y = Math.abs(50/(i-4.5))+100;
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                    bossBulletTime = game.time.now + 300;
                }
            }
        }
    },
    bossShot: function(boss, bullet){
        this.emitter.x = bullet.x;
        this.emitter.y = bullet.y;
        this.emitter.start(true, 800, null, 10);
        
        bullet.kill();
        boss.lives--;
        if(boss.lives <= 0){
            boss.kill();
            score += 1000;
            this.scoreLabel.text = "Score: "+score;
        }
    },
    setupExp: function(exp){
        exp.anchor.x = 0.5;
        exp.anchor.y = 0.5;
        exp.frame = 0;
        exp.animations.add('explosion');
    },
    pause: function(){
        if(game.paused){
            game.paused = false;
            this.pauseLabel.alpha = 0;
        }else{
            game.paused = true;
            this.pauseLabel.alpha = 1;
        }
    },
    VolUP: function(){
        this.bgm.volume += 0.1;
        this.bossBgm.volume += 0.1;
        this.explosion_player.volume += 0.1;
        volTime = game.time.now + 500;
        this.volLabel.text = "volume: "+this.bgm.volume.toPrecision(2);
    },
    VolDOWN: function(){
        this.bgm.volume -= 0.1;
        this.bossBgm.volume -= 0.1;
        this.explosion_player.volume -= 0.1;
        volTime = game.time.now + 500;
        this.volLabel.text = "volume: "+this.bgm.volume.toPrecision(2);
    },
    Ult: function(){
        if(ultimate == 100){
            ultimate = 0;
            ultimateTime = true;;
        }
    },
    bouns: function(player, box){
        if(box == this.m_box)
            player.bounsM = true;
        if(box == this.a_box)
            player.bounsA = true;
        if(box == this.h_box){
            player.bounsH = true;
            this.helper_l.reset(player.x-50, player.y-50);
            this.helper_r.reset(player.x+50, player.y-50);
        }
            
        box.kill();
    },
    fireaimingBullet: function(){
        if(aimingBulletTime < game.time.now){
            aimingBulletTime = game.time.now + 1000;
            var bullet = this.aimingBullets.getFirstDead();
            if(!bullet)
                return;
            bullet.anchor.setTo(0.5, 0.5);
            bullet.scale.setTo(2, 2);
            bullet.reset(this.player.x-50, this.player.y);
            bullet.body.velocity.y = -100;
            bullet.checkWorldBounds = true;
            bullet.outOfBoundsKill = true;
            bullet = this.aimingBullets.getFirstDead();
            if(!bullet)
                return;
            bullet.anchor.setTo(0.5, 0.5);
            bullet.scale.setTo(2, 2);
            bullet.reset(this.player.x+50, this.player.y);
            bullet.body.velocity.y = -100;
            bullet.checkWorldBounds = true;
            bullet.outOfBoundsKill = true;
        }
    },
    checkAiming: function(){
        this.aimingBullets.forEach(function(bullet){
            if(bullet.alive){
                var enemy = this.enemies.getFirstAlive();
                if(!enemy)
                    return;
                /*if(bullet.x < enemy.x)
                    bullet.body.velocity.x = 100;
                else
                    bullet.body.velocity.x = -100;
                if(bullet.y < enemy.y)
                    bullet.body.velocity.y = 100;
                else
                    bullet.body.velocity.y = -100;*/
                var deltax = bullet.x - enemy.x;
                var deltay = bullet.y - enemy.y;
                var edge = Math.sqrt(Math.pow(deltax, 2)+Math.pow(deltay, 2));
                if(deltax == 0)
                    deltax = 0.000001;
                if(deltay == 0)
                    deltay = 0.000001;
                bullet.body.velocity.x = -300/edge*deltax;
                bullet.body.velocity.y = -300/edge*deltay;
            }
        }, this);
    },
    moveHelper: function(){
        if(this.helper_l.alive){
            this.helper_l.x = this.player.x - 50;
            this.helper_l.y = this.player.y - 50;
        }
        if(this.helper_r.alive){
            this.helper_r.x = this.player.x + 50;
            this.helper_r.y = this.player.y - 50;
        }
    },
    render: function(){
        /*game.debug.cameraInfo(game.camera, 32, 32);
        game.debug.spriteCoords(this.player, 32, 500);*/
        /*game.debug.body(this.player);
        this.enemies.forEach(function(enemy){
            game.debug.body(enemy);
        });*/
    }
};