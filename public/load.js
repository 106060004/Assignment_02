var loadState = {
    preload: function(){
        //Load all game assets here
        game.load.image('background', 'assets/background.png');
        game.load.image('bg_menu', 'assets/background_menu.jpg');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemy', 'assets/enemies/Aircraft_01.png');
        game.load.image('enemyBullet', 'assets/enemies/bullet_orange0004.png');
        game.load.image('pixel_red', 'assets/pixel_red.png');
        game.load.image('pixel_orange', 'assets/pixel_orange.png');
        game.load.spritesheet('player', 'assets/images/player_03.png', 200, 200);//needed to be check
        game.load.image('boss', 'assets/enemies/Aircraft_03.png');
        game.load.image('bossBullet', 'assets/enemies/bullet_2_purple.png');
        game.load.image('bossBullet2', 'assets/enemies/bullet_blue0004.png');
        game.load.spritesheet('explosion', 'assets/explosion.png', 32, 32);

        game.load.audio('explosion_player', ['assets/explosion_hit_player.mp3', 'assets/explosion_hit_player.ogg']);
        game.load.audio('bgm', ['assets/bgm.mp3', 'assets/bgm.ogg']);
        game.load.audio('boss_bgm', ['assets/boss.mp3', 'assets/boss.ogg']);

        //bouns
        game.load.image('m_box', 'assets/m_box2.png');
        game.load.image('a_box', 'assets/a_box2.png');
        game.load.image('h_box', 'assets/h_box2.png');
        game.load.image('aimingBullet', 'assets/enemies/bullet_purple0001.png');
        game.load.image('helper', 'assets/enemies/Aircraft_10.png');
    },
    create: function(){
        //Go to the next state
        game.state.start('menu');
    }
};