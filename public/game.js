//Initailize phaser
var game = new Phaser.Game(640, 600, Phaser.AUTO, 'canvas');
//Define global variable
game.global = {
    score: 0
}
//Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
//Start the 'boot' state
game.state.start('boot');
/*
firebase.database().ref('LeaderBoard').set({
    First:{
        name: 'Tim',
        score: 0
    },
    Second:{
        name: 'AA',
        score: 0
    },
    Third:{
        name: 'QQ',
        score: 0
    }
});*/