var bootState = {
    preload: function(){

    },
    create: function(){
        game.state.backgroundColor = '#000000';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixel = true;
        //Start next state
        game.state.start('load');
    }
};